const express = require('express');
const router = express.Router()
const todolistController = require('./todolistControllers')
console.log('Connected to Router')

router.post('/new', todolistController.newList)
router.get('/', todolistController.viewList)
router.put('/edit/:listId', todolistController.editList)
router.delete('/delete/:listId', todolistController.deleteList)

module.exports = router;