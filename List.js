const mongoose = require('mongoose');

const todolistSchema = new mongoose.Schema({

	name:{
		type: String,
		required: [true, "Name is required"]
	},
	time: {
		type: String,
		required: [true, "Time is required"]
	},
	createdOn: {
		type: Date,
		default: new Date()
	}
})

module.exports = mongoose.model('List', todolistSchema)