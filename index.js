const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const app = express()
const PORT = process.env.PORT || 4000;

mongoose.connect('mongodb+srv://admin:admin123@cluster0.brekg.mongodb.net/Todolist?retryWrites=true&w=majority',
{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

db.on('error', console.error.bind(console, "MongoDB connection error"));
db.once('open', () => console.log("Connected to MongoDB"))

app.use(express.json());
app.use(cors());

const todolistRoutes = require('./todoRoute')

app.use('/todolist', todolistRoutes)



app.listen(PORT, () => console.log(`Server is running at port ${PORT}`))