const List = require('./List')
console.log("Connected to controller")

module.exports.newList = (req, res) => {

	let newList = new List({

		name: req.body.name,
		time: req.body.time
	})
	newList.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.viewList = (req, res) => {

	List.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.editList = (req, res) => {
	console.log(req.params.listId)
	let update = {
		name: req.body.name,
		time: req.body.time
	}

	List.findByIdAndUpdate(req.params.listId, update, {new: true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.deleteList = (req, res) => {

	List.findByIdAndDelete(req.params.listId, {new: true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}